import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { AngularFirestoreModule } from 'angularfire2/firestore';

export const contentHeaders = new Headers();
contentHeaders.append('Accept', 'application/json');
contentHeaders.append('Content-Type', 'application/json');
contentHeaders.append('X-Requested-With', 'XMLHttpRequest');
const CHATS_REF = "chats"
@Injectable()
export class ChatService {
  base_url = '';

  constructor(private http:Http, private db:AngularFirestoreModule) { }

  searchUser(search: any) {
    return this.http.post(this.base_url + "/chat/search/user", JSON.stringify({search: search }), { headers: contentHeaders })
      .map(res => res.json());
  }


  getUsers() {
  //  return this.http.get(this.base_url + "/chat/users")
   //   .map(res => res.json());
   return new Promise((resolve, reject)=>{
    // firebase.database().ref('${USERS_REF}/')//
   firebase.database().ref('/users/').orderByChild('uid').once('value', (snapchot)=>{
     let usersData =snapchot.val();
     let temps = []
     for(let key in usersData){
       temps.push(usersData[key])
     }
     resolve(temps);
   }).catch((err)=>{
     reject(err);
   });
 //});  
});
  }

  getGroups() {
    return this.http.get(this.base_url + "/chat/groups")
      .map(res => res.json());
  }

  getThread(uid, uid2) {
    return   this.getUnReadMessagesRef(uid, uid2)
    //return this.http.post(this.base_url + "/chat/thread", JSON.stringify(people), { headers: contentHeaders })
     //.map(res => res.json());
    // return new Promise((resolve, reject)=>{
   //    resolve([])
   //  })reu
  }
  listen(uid, uid2){
    return firebase.database().ref("chats/"+uid+"/"+uid2);
  }
  getUnReadMessagesRef(uid, uid2) {
    return new Promise((resolve, reject)=>{ firebase.database().ref("chats/"+uid+"/"+uid2).once("value", result=>{
      //console.log(result.val())
      resolve(result.val())
    })//.orderByChild('status').equalTo(GlobalStatictVar.MSG_STATUS_UN_READ);
  })
}

  saveUser(data: any) {
    return this.http.post(this.base_url + "/api/save/user", JSON.stringify(data), { headers: contentHeaders })
      .map(res => res.json());
  }

  newMessage(message){
    let currentUserRef = firebase.database().ref("chats/"+message.to+"/"+message.from);
    let sec = firebase.database().ref("chats/"+message.from+"/"+message.to);
    return currentUserRef.push(message) && sec.push(message);
  }

 

}
