import { Injectable, EventEmitter} from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import "rxjs/add/operator/do";
import { map } from 'rxjs/operators/map';
import { Router }    from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { AngularFirestoreModule } from 'angularfire2/firestore';



export const contentHeaders = new Headers();
contentHeaders.append('Accept', 'application/json');
contentHeaders.append('Content-Type', 'application/json');
contentHeaders.append('X-Requested-With', 'XMLHttpRequest');

@Injectable()
export class AuthService {
    private static auth: AuthService;
    private _emitter: EventEmitter<any> = new EventEmitter();
    private _init: Promise<boolean>;
    private _user: any;
    private base_url = 'https://cdi.x10.mx/api';
  private userId;
    constructor(private router: Router, private http: Http, public afAuth: AngularFireAuth, public database: AngularFirestoreModule) {
      if (AuthService.auth) {
        throw new Error("AuthService service is already initialized.");
      }
      
      this.router = router;
      this.http = http;
      AuthService.auth = this;

    this._init = new Promise<boolean>((resolve, reject) => {
        this.http.get(this.base_url + "/user")
          .map(res => res.json())
          .subscribe(data => {
              this._user = data.user;
              this.emit();
              resolve(true);
          }, reject);
      });
    
     
    /*  this._init =  new Promise<boolean>((resolve, reject) => {*/
      this.afAuth.authState.subscribe((auth:any) => {
        if (auth) {
          console.log("auth",auth)
          this.userId = auth.uid
          let usr :any;
          //usr.uid;
          usr = auth.providerData[0];
          //usr.uid = this.userId
          this._user = auth;
          
          this.emit();
         
          this.updateOnConnect()
          this.updateOnDisconnect()
        } else {
          
        }
      });
      
  /*  });
*/

      
    }

    subscribe(next: (user: any) => void) : any {
      let listener = this._emitter.subscribe(next);
      next(this.user);
      return listener;
    }
    private updateStatus(status: string) {
      if (!this.userId) return
      firebase.database().ref(`users/` + this.userId).update({ status: status })
    }
    private updateOnConnect() {
      let connected = firebase.database().ref('.info/connected');
      connected.once("value", (val:any)=>{
        let status = val.$value ? 'online' : 'offline'
        this.updateStatus(status)
      })
    /*  return this.db.database.ref('.info/connected').
      
                    .do(connected => {
                        let status = connected.$value ? 'online' : 'offline'
                        this.updateStatus(status)
                    })
                    .subscribe()*/
    }
    private updateOnDisconnect() {
      firebase.database().ref().child(`users/${this.userId}`)
              .onDisconnect()
              .update({status: 'offline'})
    }
    static check(): Promise<boolean> {
      return new Promise<boolean>((resolve, reject) => {
        function doCheck() {
          if (!AuthService.auth.user) {
           AuthService.auth.router.navigate(['/login']);
          }

          resolve(!!AuthService.auth.user);
        }
        AuthService.auth._init.then(doCheck, doCheck);
      });
    }

    static checkUnauth(): Promise<boolean> {
      return new Promise<boolean>((resolve, reject) => {
        function doCheck() {
          if (AuthService.auth.user) {
            AuthService.auth.router.navigate(['/home']);
          }
          resolve(!AuthService.auth.user);
        }
        AuthService.auth._init.then(doCheck, doCheck);
      });
    }

    get user() {
      return this._user;
    }


    login(username: string, password: string) {
      let body = JSON.stringify({ username, password });
      return new Promise<boolean>((resolve, reject) => {
        this.http.post(this.base_url + "/auth/login", body, { headers : contentHeaders })
          .map(res => res.json())
          .subscribe(data => {
            this._user = data.user;
            console.log(data.user)
            this.emit();
            return resolve();
          }, err => {
            console.log("login error", err);
            return reject();
          });
        });
    }
    loginWithEmail(email:string, password:string){
      return new Promise((resolve, reject) => {
        this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((value =>{
        this.getUserDetails().then((user:any)=>{
          user.displayName= user.firstName+" "+user.lastName;
          this._user = user;
          firebase.database().ref('/photos').child(user.uid).update({photoURL:user.photoURL, email:email})
          this.emit();
          return resolve(true);
        }).catch(err=>{
          reject(err)
        })
      }))
      .catch((err)=>{
        let errCode = err.code
      })
    })
  }
  getPhotoURL(email){
   let photos = firebase.database().ref('/photos')
  return new Promise((resolve, reject)=>{
    photos.orderByChild('email').equalTo(email).limitToFirst(1).once('value', (snapshot)=>{
      resolve(snapshot.val());
    }).catch((err)=>{
      resolve({photoURL:"assets/cedi.png"});
    })
  });
  }
    fireUserData = firebase.database().ref('/users');
    getUserDetails(){
      return new Promise((resolve, reject)=>{
        this.fireUserData.child(firebase.auth().currentUser.uid).once('value', (snapshot)=>{
          resolve(snapshot.val());
        }).catch((err)=>{
          reject(err);
        })
      });
    }
    
    loginTest(){
      this._user= {username:"ISMOBAGA"}
    }
    logout() {
     /* this.http.get(this.base_url + "/auth/logout")
      .subscribe(res => {
        this._user = null;
        this.emit();
        this.router.navigate(['/login']);
      });*/
      this.afAuth.auth.signOut().then(res=>{

        this.takeMeLogin();
      });
    }

    takeMeLogin() {
      this._user = null;
      this.emit();
      this.router.navigate(['/login']);
    }


    private emit() {
      this._emitter.emit(this.user);
    }
  }
