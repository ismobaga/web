import { ModuleWithProviders }  from '@angular/core';
import { Routes,
         RouterModule, CanActivate } from '@angular/router';
         import { AppComponent } from './app.component';
         import { HomeComponent } from './components/home/home.component';
         import { LoginComponent } from './components/login/login.component';
         import { RegisterComponent } from './components/register/register.component';
         import { loginRoutes,
            authProviders }      from './components/login/login.routing';
   import { AuthGuard } from './services/auth/auth-guard.activate.service';
   import { AuthCheckLoginGuard } from './services/auth/auth-guard.desactivate.service';
   import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component'
/* import { loginRoutes,
         authProviders }      from './common/login/login.routing';
import { AuthGuard } from './services/auth/auth-guard.activate.service';
import { AuthCheckLoginGuard } from './services/auth/auth-guard.deactivate.service';
 */
export const appRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent, canActivate: [AuthCheckLoginGuard] },
    { path: 'register', component: RegisterComponent, canActivate: [AuthCheckLoginGuard] },
    { path: '**', component: PageNotFoundComponent },
    ...loginRoutes
  ];
  

export const appRoutingProviders: any[] = [
  authProviders
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
