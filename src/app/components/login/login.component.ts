import { Component, OnInit } from '@angular/core';
import { Router }      from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

import * as $ from 'jquery';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  error = false;
  signing = false;
  errorMessage: any;
   photoURL="assets/cedi.png"
  userEmail:string;

  constructor(public authService: AuthService, public router: Router) { }

  ngOnInit() {
  }
  emailChanged(email:string){

    if(email.length<=5)
    return ;
    if(!email.includes("@"))
      email = email+"@umoncton.ca"

    this.authService.getPhotoURL(email).then((res)=>{
      console.log(res)
      if(res){
        for(var i in res){
          this.photoURL = res[i].photoURL;
          break;
        }
      }
      
      else{
        this.photoURL="assets/cedi.png"
      }
    //  this.photoURL = res.photoURL;
    }).catch(err=>{console.log(err)})
    
  }

  login(e: Event, username: string, password: string) {
    if(!username.includes("@"))
    username = username+"@umoncton.ca"
    e.preventDefault();
    this.signing = true;
    this.authService.loginWithEmail(username, password).then( res => {
      this.error = this.signing = false;
      $('.form').fadeOut(500);
      $('.wrapper').addClass('form-success');
      setTimeout(() => {
        this.router.navigate(['/home']);
      }, 1500);
    }).catch( err => {
      this.errorMessage = "Nom d'utilisateur ou mot de PASSE invalide"
      this.error = true;
      console.log(err)
      this.signing = false;
    });
  }
loginTest(e: Event, username: string, password: string){
  this.error = this.signing = false;
  $('.form').fadeOut(500);
  $('.wrapper').addClass('form-success');
  setTimeout(() => {
    this.router.navigate(['home']);
  }, 1500);
}
  logout() {
    this.authService.logout();
  }

}
