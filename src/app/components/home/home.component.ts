import { Component, ElementRef, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import {Observable} from 'rxjs/Observable';
import { AuthService } from '../../services/auth/auth.service';
import { ChatService } from '../../services/chat/chat.service';
import { Broadcaster } from '../../utils/broadcaster';

import * as io from 'socket.io-client';
import * as _ from 'lodash';
import * as $ from 'jquery';

const rooms = {};
String.prototype.trim = function () {
  return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.css' ]
})
export class HomeComponent {
  _user: any;
  _users: any;
  _groups: any;
  _onlineUsers: any = [];
  draftMessage: any;
  currentThread: any;
  receiverId: any;
  searchUsers: any;
  searchDraft = new FormControl();
  textareaDraft = new FormControl();
  isSearchDraft: boolean = false;
  isLoadingUser: boolean = false;
  isTyping: boolean = false;
  colorSet: any = ['#ba6236', '#ae7313', '#a5980d', '#7d9726', '#5b9d48', '#36a166', '#9d6c7c', '#0e5a94', '#9d6c7c', '#5e6e5e'];
  homeImage: any = "assets/cedi.png";
 // public socket = io.connect('');
  constructor(private auth: AuthService,public events: Broadcaster, private _chatService: ChatService, public el: ElementRef) {
    auth.subscribe((user) => {
      this._user = user;
    });
   
    this.searchDraft.valueChanges
      .debounceTime(1500)
      .distinctUntilChanged()
      .subscribe(newValue => {
        if (newValue) {
          this.isSearchDraft = true;
          this.isLoadingUser = true;
         // this._chatService.searchUser(newValue)
         this.searchUser1(newValue)
            .then((res:any) => {
  
              this.searchUsers = res;
              this.isLoadingUser = false;
            }, err => {
              if (err.status === 401) { this.auth.takeMeLogin(); }
            });
        } else {
          this.isSearchDraft = false;
        }
      });

    this.textareaDraft.valueChanges
      .debounceTime(2000)
      .distinctUntilChanged()
      .subscribe(newValue => {
        if (newValue) {
          let data = {
            roomId: this.currentThread.id,
            receiverId: this.receiverId,
            typing: true
          };
        //  this.socket.emit("on:typing-message", data);
          this.events.broadcast("typing-message", data)
        } else {
          this.isTyping = false;
        }
      });

  //  this.socket.emit("on:login", this._user.id);
    this.events.broadcast("login", this._user)
    this.events.on<string>("login").subscribe(data=>{
   // this.socket.on("on:login", (data: any) => {
      this._onlineUsers = data;
    });
    this.events.on<string>("send-message").subscribe((message:any) => {
     
      if(this._user.uid==message.from)
      return ;
      console.log("new message from ", message.senderName)
      let roomId = message.to;
      let room = rooms[roomId];
      let senderUID = message.uid;
      if (!room) {
        rooms[roomId] = {
          unread:0,
          messages: [ message ],
          roomId: roomId,
          people: [ senderUID, roomId ]
        };
      }
      if (rooms[roomId]) {
        let aasx =this.hack(rooms[this.currentThread.id].messages);
        
         aasx.push(message);
         rooms[roomId].messages =aasx
        ///console.log(rooms[roomId].messages)
      }
      if (this.currentThread) {
        this.scrollToBottom();
        if (String(roomId) !== String(this.currentThread.uid)) {
          rooms[roomId].unread = rooms[roomId].unread + 1;
        }
      } else {
        rooms[roomId].unread = rooms[roomId].unread + 1;
      }
      let index = this._users.indexOf(this._users.filter((el: any) => {
        return (String(el.uid) === String(senderUID));
      })[0]);
      if (index === -1) {
        let newUser = {
          photoURL: message.photoURL,
          uid: message.uid,
          displayName: message.displayName
        }
        this._users.unshift(newUser);
      }
      
    });
    this.events.on<string>("is-typing").subscribe((data:any)=>{
   // this.socket.on("on:is-typing", (data: any) => {
      if (data.typing) {
        
        this.isTyping = true;
        this.scrollToBottom();
      } else {
        this.isTyping = false;
      }
    });

 //   this.socket.on("on:joined-room", (data: any) => {
      this.events.on<string>("joined-room").subscribe((data:any)=>{
      let roomId = data.roomId;
      let room = rooms[roomId];
      if (!room) {
        rooms[roomId] = data;
        rooms[roomId].unread = 0;
      }
      if (room) {
        room.messages = data.messages;
        room.unread = 0;
      }
    });

  //  this.socket.on("on:joined-group-room", (data: any) => {
      this.events.on<string>("joined-group-room").subscribe((data:any)=>{
              let roomId = data.roomId;
      let room = rooms[roomId];
      if (!room) {
        rooms[roomId] = data;
      }
      if (room) {
        rooms[roomId].messages = data.messages;
      }
    });
  }
  leftSideBar:boolean = false;
  toggleLeftSideBar(event:any) {
    event.preventDefault();
    if(this.leftSideBar)
      {
        document.getElementById("sidebar-wrapper").style.width = "280px";
        document.getElementById("wrapper").style.paddingLeft = "10px";
    }
    else{
      document.getElementById("wrapper").style.paddingLeft = "10px";
      document.getElementById("sidebar-wrapper").style.width = "0px";
    }
    
    this.leftSideBar = !this.leftSideBar;
  }
  searchUser1(val){
   return  new Promise((resolve, reject) => {
      let contacts = new Array();
    if (val && val.trim() != '') {
      
      //filter users contacts ( LocalStorage )
      this.users.filter((item) => {
        if (item.displayName.toLowerCase().includes(val.toLowerCase())) {
          contacts.push(item);
        }
      });
  }
  resolve(contacts);
});
}

  ngOnInit() {
    this.getAllUsers();
  }

  get user() : any {
    return this._user;
  }

  get users() : any {
    return this._users;
  }

  get groups() : any {
    return this._groups;
  }

  get messages() : Array<Object> {
    if (!rooms[this.currentThread.id]) {
      return [];
    }
    return rooms[this.currentThread.id].messages;
  }

  isOnline(user: any) : any {
    //return this._onlineUsers.includes(user);
    
    return user.status=="online";
  }

  getUnreadMessages(userId: any) {
    let unread:any = null;
   // return new Promise((resolve, reject)=>{
    //this._chatService.getUnReadMessagesRef(this._user.uid, this.user.uid).then((result:any)=>{

    
    
     // let people = rooms[room].people;
     // if (people.includes(userId) && people.includes(this._user.id) && rooms[room].unread !== 0) {
       //console.log(result)
     //  console.log(result)
     let result =null
      if ( result) {
        //console.log(result)
        return(result);
      
    }
    else{
      return(unread);
    }
 // })

//})
   
  }


  selectSearchUser(e: Event, user: any) {
    let index = this._users.indexOf(this._users.filter((el: any) => {
      return (String(el.id) === String(user.id));
    })[0]);
    if (index === -1) {
      this._users.unshift(user);
    }
    this.selectThread(e, user);
    this.isSearchDraft = false;
  }
messag
  selectThread(event: any, thread: any) {
    event.preventDefault();
    console.log(thread)
    this.receiverId = thread.uid;
    let people = {
      sender: this._user.uid,
      receiver: thread.uid
    };7
    this._chatService.getThread(this._user.uid, thread.uid)
      .then((res:Array<Object>) => {
        this.currentThread = {
          id: thread.uid,
          photoURL: thread.photoURL,
          displayName: thread.displayName,
          people: [ thread.uid, this._user.uid]
        //  people: res.people
        };
        if(!rooms[this.currentThread.id])
        rooms[this.currentThread.id] = []

        //rooms[this.currentThread.id].messages ;
        
        rooms[this.currentThread.id].messages = res
        this.listenForNew(this._user.uid, thread.uid)
        //this.messag = res;
        
        //this.socket.emit("on:join-room", this.currentThread);
        this.events.broadcast("join-room",  this.currentThread)
      }, err => {
        if (err.status === 401) { this.auth.takeMeLogin(); }
      });
  }
  listenForNew(uid, uid2) {
     this._chatService.listen(uid, uid2).on('child_added', (child:any) => {
      
      
      var obj = child.val();
      if(obj.from!=this._user.uid){

         
      
      console.log("new nessage");
      obj.key = child.key;
      let aasx =this.hack(rooms[this.currentThread.id].messages);
      aasx.push(obj);
      rooms[this.currentThread.id].messages = aasx
    }
  });
}

  selectGroup(event: any, thread: any) {
    event.preventDefault();
    this.currentThread = {
      id: thread.id,
      avatar: thread.avatar,
      fullName: thread.name
    };
    this.events.broadcast("join-group-room", this.currentThread);
  }
hack(val){
  if(!val)
    return [];
  return Object.keys(val).map(key => val[key]);
}

  onEnter(event: any): void {
    event.preventDefault();
    let data = {
      roomId: this.currentThread.id,
      receiverId: this.receiverId,
      typing: false
    };
    //this.socket.emit("on:typing-message", data);
    this.events.broadcast("typing-message", data)
    let today = new Date().toISOString();
    this.draftMessage = this.draftMessage.trim()
    let random = Math.floor(Math.random() * 10);
    if (!this.draftMessage) {
      return;
    }
  
    let id1 = this.currentThread.people[0];
    let id2 = this.currentThread.people[1];
    let rid = id1 === this._user.uid ? id2 :  id1;
    if (this.currentThread) {
     
      let message = {
        roomId: this.currentThread.id,
        senderId: this._user.uid,
        senderName: this._user.displayName,
        senderAvatar: this._user.photoURL,
        receiverId: rid,
        from:this._user.uid,
        to: this.currentThread.id,
        body: this.draftMessage,
        sentAt: today,
        datetime: new Date(),
        meta: this.colorSet[random],
        type:0
      };
      this.draftMessage = null;
      if (rooms[this.currentThread.id]) {
        let aasx =this.hack(rooms[this.currentThread.id].messages);
        
         aasx.push(message);
         rooms[this.currentThread.id].messages = aasx
        //rooms[this.currentThread.id].messages = this.hack(rooms[this.currentThread.id].messages).push(message);
        this.scrollToBottom();
      }
      //this.socket.emit("on:send-message", message);
      this.events.broadcast("send-message", message);
      this._chatService.newMessage(message);
    }
  }

  getAllUsers() {
    this.isLoadingUser = true;
    this._chatService.getUsers()
      .then(res => {
        this._users = res;
        this.isLoadingUser = false;
      }, err => {
        if (err.status === 401) { this.auth.takeMeLogin(); }
      });
  }

  scrollToBottom(): void {
    var $t = $('#chat-history');
    $t.animate({"scrollTop": $('#chat-history')[0].scrollHeight}, "swing");
  }

  logout(e: Event) {
    e.preventDefault();
    this.events.broadcast("on:logout", this._user.id);
    this.auth.logout();
  }
}
